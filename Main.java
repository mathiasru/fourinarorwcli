package com.company;

import java.util.Scanner;

/**
 * Vier Gewinnt für 2 Spieler
 *
 * @author mathiasrudig
 * @version 1.0
 */
public class Main {

    /**
     * Main-Methode die das Spiel startet und die Größe des Spielfeldes wird definiert
     */
    public static void main(String[] args) {
        int zeile;
        int spalte;

        Scanner scan = new Scanner(System.in);
        System.out.println("********************** 4 Gewinnt ************************");
        System.out.println();
        System.out.println("Möchten Sie das Spiel mit 6x7 [1] oder 10x10 [2]: ");
        System.out.print("Eingabe 1 oder 2:");
        String eingabe = scan.nextLine();
        switch (eingabe){
            case ("1"):
                zeile = 6;
                spalte = 7;
                break;
            case ("2"):
                zeile = 10;
                spalte = 10;
                break;
            default:
                System.out.println("Falsche Eingabe, es wird 6x7 gewählt!");
                zeile = 6;
                spalte = 7;
        }
        System.out.println();
        System.out.println("****************** Spiel wird gestartet *****************");
        System.out.println();
        //Spiel starten und Spielfeldgröße übermitteln
        Spiel neuesSpiel = new Spiel(zeile, spalte);
        neuesSpiel.initialisiereSpielmenue(neuesSpiel.getZeile(), neuesSpiel.getSpalte());
    }
}
