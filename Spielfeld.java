package com.company;

public class Spielfeld {
    //Datenfelder
    private String[][] feld; //mehrdimensionaler Array
    private Spieler spieler;

    /**
     * Konstruktor der Klasse Spielfeld, weißt dem mehrdimensionalen Array, am Start den Wert " " zu.
     *
     * @param x übergebene Anzahl der Reihen
     * @param y übergebene Anzahl der Spalten
     */
    //könnte man sich auch sparen, da der Defaultwert schon 0 wäre
    public Spielfeld(int x, int y) {
        feld = new String[x][y];
        for (int reihe = 0; reihe < feld.length; reihe++) { //erste Dimension
            for (int spalte = 0; spalte < feld[reihe].length; spalte++) { //zweite Dimension
                feld[reihe][spalte] = " ";
            }
        }
    }

    /**
     * Mit dieser Methode wird der Spielstein des jeweiligen Spielers gesetzt.
     *
     * @param spielerZeichen übergebene Spielernummer
     * @param spalte         übergebene Spaltennummer (Koordinate y)
     */
    public boolean setzeSpielstein(String spielerZeichen, int spalte, Spieler spieler1, Spieler spieler2) {
        if (feld[0][spalte].equals(spieler1.getSpielerZeichen()) || feld[0][spalte].equals(spieler2.getSpielerZeichen())) {
            System.out.println();
            System.out.println("Der Stein konte nicht gesetzt werden!");
            System.out.println("Spalte ist voll!");
            return false;
        }
        //Variable zum überprüfen der nächsten freien Reihe innerhalb der übergeben Spalte
        int reihe = feld.length - 1;
        boolean fortsetzen = true;
        while (reihe >= 0 && fortsetzen) {
            if (feld[reihe][spalte].equals(" ")) {
                feld[reihe][spalte] = spielerZeichen;
                fortsetzen = false;
            }
            reihe--; //rückt immer in die freie Zeile
        }
        return true;
    }

    /**
     * Gibt das Spielfeld mittels des mehrdimensionalen Arrays über zwei verschachtelte for-Schleifen aus.
     * Zudem wurd auch noch eine Beschriftung der Spalten ausgegeben.
     */
    public void ausgabeSpielfeld() {
        //Beschriftung der Spalten
        for (int i = 1; i <= feld[0].length; i++) {
            System.out.printf("%3d",i);
        }
        System.out.println();
        System.out.println();
        //Spielfelausgabe
        for (int reihe = 0; reihe < feld.length; reihe++) { //erste Dimension
            for (int spalte = 0; spalte < feld[reihe].length; spalte++) { //zweite Dimension
                System.out.printf("|%2s",feld[reihe][spalte]);
                if (spalte == feld[reihe].length - 1) {
                    System.out.print("|");
                }
            }
            System.out.println();
        }
    }

    /**
     * Prüft ob die Anzahl der möglichen Züge mit den Feldern übereinstimmen und somit ein Unentschieden das Ergebnis wäre
     *
     * @return gibt bei Erfüllen der Bedingung einen boolschen Wert true zurück
     */
    public boolean unentschieden() {
        boolean returnwert = false;
        //Prüfung ob alle möglichen Steine gelegt wurden
        if (getAnzahlGesetzterSteine() == feld.length * feld[0].length) {
            returnwert = true;
        }
        return returnwert;
    }

    /**
     * Zählt nach jedem Zug die bisher gesetzten Steine auf dem Spielfeld, und vergleicht mit den möglichen Zügen
     *
     * @return gibt die Anzahl der bisher gelegten Steine zurück
     */
    public int getAnzahlGesetzterSteine() {
        int anzSteine = 0;
        for (int reihe = 0; reihe < feld.length; reihe++) { //erste Dimension
            for (int spalte = 0; spalte < feld[reihe].length; spalte++) { //zweite Dimension
                if (!feld[reihe][spalte].equals(" ")) {
                    anzSteine++;
                }
            }
        }
        return anzSteine;
    }

    /**
     * Ruft alle Prüfbedingungen auf und dient zur Ermittlung des Gewinners
     *
     * @param spielerZeichen übergebene Spielerzeichen
     * @return gibt einen boolscher Wert zurück, der nach Erfüllung der Bedingung auf true gesetzt wird
     */
    public boolean testeReihe(String spielerZeichen) {
        boolean returnwert = false;
        //Komplettes Feld mittels der 3 Methoden prüfen
        for (int reihe = 0; reihe < feld.length; reihe++) { //erste Dimension
            for (int spalte = 0; spalte < feld[reihe].length; spalte++) { //zweite Dimension
                if (testeHorizontal(reihe, spalte, spielerZeichen) || testeVertikal(reihe, spalte, spielerZeichen) || testeQuer(reihe, spalte, spielerZeichen)) {
                    returnwert = true;
                }
            }
        }
        return returnwert;
    }

    /**
     * Prüft das Spielfeld vertikal auf vier gleiche aufeinanderfolgende Zeichen (spielerZeichen)
     *
     * @param reihe          übergebene Anzahl der Reihen
     * @param spalte         übergebene Anzahl der Spalten
     * @param spielerZeichen übergebene Spielerzeichen
     * @return gibt einen boolscher Wert zurück, der nach Erfüllung der Bedingung auf true gesetzt wird
     */
    public boolean testeVertikal(int reihe, int spalte, String spielerZeichen) {
        boolean returnwert = false;
        //Prüfung, ob vier ab dieser Stelle überhaupt möglich sind, sonst wird über das Feld hinaus geprüft und ein Fehler ausgelöst
        if (reihe + 3 < feld.length) {
            //Prüfung auf vier gleiche aufeinanderfolgende Zahlen (spielerNr)
            if (feld[reihe][spalte].equals(spielerZeichen) && feld[reihe + 1][spalte].equals(spielerZeichen) && feld[reihe + 2][spalte].equals(spielerZeichen) && feld[reihe + 3][spalte].equals(spielerZeichen)) {
                returnwert = true;
            }
        }
        return returnwert;
    }

    /**
     * Prüft das Spielfeld horizontal auf vier gleiche aufeinanderfolgende Zeichen (spielerZeichen)
     *
     * @param reihe          übergebene Anzahl der Reihen
     * @param spalte         übergebene Anzahl der Spalten
     * @param spielerZeichen übergebenes Spielerzeichen
     * @return gibt einen boolscher Wert zurück, der nach Erfüllung der Bedingung auf true gesetzt wird
     */
    public boolean testeHorizontal(int reihe, int spalte, String spielerZeichen) {
        boolean returnwert = false;
        //Prüfung, ob vier von dieser Stelle aus überhaupt möglich sind
        if (spalte + 3 < feld[reihe].length) {
            //Prüfung auf vier gleiche aufeinanderfolgende Zahlen (spielerNr)
            if (feld[reihe][spalte].equals(spielerZeichen) && feld[reihe][spalte + 1].equals(spielerZeichen) && feld[reihe][spalte + 2].equals(spielerZeichen) && feld[reihe][spalte + 3].equals(spielerZeichen)) {
                returnwert = true;
            }
        }
        return returnwert;
    }


    /**
     * Prüft das Spielfeld quer auf vier gleiche aufeinanderfolgende Zeichen (spielerZeichen)
     *
     * @param reihe          übergebene Anzahl der Reihen
     * @param spalte         übergebene Anzahl der Spalten
     * @param spielerZeichen übergebenes Spielerzeichen
     * @return gibt einen boolscher Wert zurück, der nach Erfüllung der Bedingung auf true gesetzt wird
     */
    public boolean testeQuer(int reihe, int spalte, String spielerZeichen) {
        boolean returnwert = false;
        //Prüfung, ob vier ab dieser Stelle überhaupt möglich sind, sonst wird über das Feld hinaus geprüft und ein Fehler ausgelöst
        if (reihe + 3 < feld.length && spalte + 3 < feld[0].length) {
            //Prüfung auf vier gleiche aufeinanderfolgende Zahlen (spielerNr)
            if (feld[reihe][spalte].equals(spielerZeichen) && feld[reihe + 1][spalte + 1].equals(spielerZeichen) && feld[reihe + 2][spalte + 2].equals(spielerZeichen) && feld[reihe + 3][spalte + 3].equals(spielerZeichen)) {
                returnwert = true;
            }
        }
        //Prüfung, ob vier ab dieser Stelle überhaupt möglich sind, sonst wird über das Feld hinaus geprüft und ein Fehler ausgelöst
        if (reihe - 3 >= 0 && spalte + 3 < feld[0].length) {
            //Prüfung auf vier gleiche aufeinanderfolgende Zahlen (spielerNr)
            if (feld[reihe][spalte].equals(spielerZeichen) && feld[reihe - 1][spalte + 1].equals(spielerZeichen) && feld[reihe - 2][spalte + 2].equals(spielerZeichen) && feld[reihe - 3][spalte + 3].equals(spielerZeichen)) {
                returnwert = true;
            }
        }
        return returnwert;
    }
}
