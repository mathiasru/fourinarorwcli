package com.company;

public class Spieler {
    //Datenfelder
    private String name;
    private int spielerNr;
    private String spielerZeichen;

    /**
     * Konstruktor (überladen) der Klasse Spieler
     *
     * @param name           übergebener Spielerame
     * @param spielerZeichen übergebene Spielernummer
     */
    public Spieler(String name, int spielerNr, String spielerZeichen) {
        //Reihenfolge darf nicht geändert werden, damit die Spielernummer bei Nichteingabe eines Namens richtig gesetzt wird
        setSpielerNr(spielerNr);
        setName(name);
        setSpielerZeichen(spielerZeichen);
    }

    /**
     * Getter-Methode für den Spielernamen
     *
     * @return gibt den Spielernamen zurück
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter-Methode für den Spielernamen
     *
     * @param name übergebener Spielerame
     */
    public void setName(String name) {
        if (name.equals("")) {
            this.name = "Spieler " + getSpielerNr();
        } else {
            this.name = name;
        }
    }

    /**
     * Getter-Methode für die Spielernummer
     *
     * @return gibt die Spielernumer zurück
     */
    public int getSpielerNr() {
        return this.spielerNr;
    }

    /**
     * Setter-Methode für die Spielernumer
     *
     * @param spielerNr übergebener Spielernumer
     */
    public void setSpielerNr(int spielerNr) {
        this.spielerNr = spielerNr;
    }

    /**
     * Getter-Methode für das Spielerzeichen
     * @return spielerZeichen
     */
    public String getSpielerZeichen() {
        return spielerZeichen;
    }

    /**
     * Setter-Methode für das Spielerzeichen
     * @param spielerZeichen übergebendes Spielerzeichen
     */
    public void setSpielerZeichen(String spielerZeichen) {
        this.spielerZeichen = spielerZeichen;
    }
}

