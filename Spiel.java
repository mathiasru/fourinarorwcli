package com.company;

import java.util.Scanner;

public class Spiel {
    //Datenfelder
    private int zeile;
    private int spalte;

    /**
     * Überladener Konstruktor der Klasse Spiel
     *
     * @param zeile  übergebene Anzahl der Zeilen
     * @param spalte übergebene Anzahl der Spalten
     */
    public Spiel(int zeile, int spalte) {
        setZeile(zeile);
        setSpalte(spalte);
    }

    //Getter und Setter Methoden
    /**
     * Setter-Methode für die Anzahl der Zeilen, falls das Feld zu klein/groß gewählt wurde, wird es per default auf 6 gesetzt
     * @param zeile übergebene Anzahl
     */
    public void setZeile(int zeile) {
        if (zeile < 6 || zeile > 20){
            this.zeile = 6;
        }else {
            this.zeile = zeile;
        }
    }
    /**
     * Setter-Methode für die Anzahl der Spalten, falls das Feld zu klein/groß gewählt wurde, wird es per default auf 7 gesetzt
     * @param spalte übergebene Anzahl
     */
    public void setSpalte(int spalte) {
        if (spalte < 7 || spalte > 20){
            this.spalte = 7;
        }else{
            this.spalte = spalte;
        }
    }

    /**
     * Getter-Methode Anzahl der Spalten
     * @return spalte
     */
    public int getSpalte() {
        return this.spalte;
    }

    /**
     * Getter-Methode Anzahl der Zeilen
     * @return zeile
     */
    public int getZeile() {
        return this.zeile;
    }

    //Methoden
    /**
     * Methode, die das Gameplay beinhaltet
     *
     * @param zeile  übergebene Anzahl der Zeilen
     * @param spalte übergebene Anzahl der Spalten
     */
    public void initialisiereSpielmenue(int zeile, int spalte) {
        //Spielernamen einlesen
        Scanner scan = new Scanner(System.in);
        System.out.println("Wenn keine Namen vergeben werden, wird ein default Name verwendet!");
        System.out.print("Bitte geben Sie den Namen von Spieler 1 ein: ");
        String spielerName1 = scan.nextLine();
        System.out.print("Bitte geben Sie den Namen von Spieler 2 ein: ");
        String spielerName2 = scan.nextLine();
        System.out.println();

        //Spielfeld erzeugen
        Spielfeld spielfeld = new Spielfeld(zeile, spalte);

        //Spieler erzeugen
        Spieler spieler1 = new Spieler(spielerName1, 1, "X");
        Spieler spieler2 = new Spieler(spielerName2, 2, "O");

        //aktuellen Spieler definieren (Referenz)
        Spieler aktuellerSpieler = spieler1;

        // Spielfeld ausgeben und diverse Überprüfungen (Stein platzieren, nächster Spieler, überprüfung der Reihen)
        while (!spielfeld.unentschieden() && !spielfeld.testeReihe(aktuellerSpieler.getSpielerZeichen())) {
            System.out.println();
            spielfeld.ausgabeSpielfeld();
            System.out.println();
            System.out.println(aktuellerSpieler.getName() + ", Sie sind an der Reihe!");
            System.out.println("Bitte wählen Sie die Spalte aus! (Wert zwischen 1 und 7)");
            System.out.print("Eingabe: ");

            int eingabe = pruefeEingabe(scan.nextInt());

            //Stein des aktuellen Spielers setzen und prüfen, ob die Spalte voll ist
            //Falls die Spalte voll ist wird der Spieler solange aufgefordert, bis eine freie Spalte gewählt wurde
            while(!spielfeld.setzeSpielstein(aktuellerSpieler.getSpielerZeichen(), eingabe, spieler1, spieler2)){
                System.out.println("Eingabe: ");
                eingabe = pruefeEingabe(scan.nextInt());
            }
            //zusätzliche Überprüfung der Reihen, damit im Falle eines Gewinnes kein Spielerwechsel mehr stattfindet
            if (!spielfeld.testeReihe(aktuellerSpieler.getSpielerZeichen())) {
                //Hier wird ermittelt welcher Spieler als nächstes an ie Reihe kommt
                if (aktuellerSpieler.getSpielerNr() == 1) {
                    aktuellerSpieler = spieler2;
                } else {
                    aktuellerSpieler = spieler1;
                }
            } else {
                System.out.println();
                spielfeld.ausgabeSpielfeld();
                System.out.println();
            }
        }
        System.out.println("Herzlichen Glückwunsch, " + aktuellerSpieler.getName() + " hat gewonnen!!");
        scan.close();
        beendeSpiel();
    }

    /**
     *  Überprüft die Eingabe des Benutzers und gibt den Wert zurück
     *
     * @param eingabe Übergebene Eingabe des Benutzers bezüglich der Spalte
     * @return gibt die überprüfte Eingabe des Benutzers zurück
     */
    public int pruefeEingabe(int eingabe){
        //Hier wird die Gültigkeit der Position des Steines ermittelt
        Scanner scan = new Scanner(System.in);
        while(eingabe <= 0 || eingabe > spalte) {
            System.out.println("Eingabe nicht gültig, bitte versuchen Sie es noch einmal!");
            System.out.println("Eingabe: ");
            eingabe = scan.nextInt();
        }

        //Umrechnung damit der Index für das Array stimmt
        eingabe -= 1;
        return eingabe;
    }

    /**
     * Statische Klassen-Methode, die das Spiel beendet
     */
    public static void beendeSpiel() {
        System.exit(0);
    }
}
